define(['./module'], function (controllers) {
    'use strict';
    controllers.controller('fsController', ['$scope', '$rootScope', '$log', '$interval', '$http', 'noty', '$sce', '$compile',
        function($scope, $rootScope, $log, $interval, $http, noty, $sce, $compile) {

            $scope.noty = noty; // notify service

            $scope.fsEntries = [];
            $scope.currentPath = '';
            $scope.fileName = '';
            $scope.selectedFileSize = -1;
            $scope.MAX_FILE_SIZE = 50000000;
            $scope.MINUTES_IN_DAY = 1440;
            $scope.file = '';
            var REST_URL = 'rest/fs/';
            var GET_LIST_URL = REST_URL + 'ls';
            var GET_CURR_PATH_URL = REST_URL + 'dirName';
            var GO_TO_PARENT_FOLDER_URL = REST_URL + 'up';
            var CHANGE_DIR_URL = REST_URL + 'cd/';
            var READ_FILE_URL = REST_URL + 'read/';
            var GET_FILE_DIFF_PUSH = 'getFileDiff';
            var GET_SELECTED_FILE_NAME_URL = REST_URL + 'selectedFile';

            // polling
            var GET_FILE_DIFF_POLL = REST_URL + 'diff';
            var POLLING_INTERVAL = 3000;
            $scope.intervalPromise = null;

            $scope.loadData = function() {
                $http.get(GET_LIST_URL).success(function(data) {
                    $scope.fsEntries = data;
                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                });

                $http.get(GET_CURR_PATH_URL).success(function(data) {
                    $scope.currentPath = $scope.removeJsonQuotes(data);
                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                });
            };

            $scope.goUp = function() {
                $http.get(GO_TO_PARENT_FOLDER_URL).success(function(data) {
                    $scope.loadData();
                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                });
            };

            $scope.changeDir = function(folder) {
                $http.get(CHANGE_DIR_URL + folder).success(function(data) {
                    $scope.loadData();
                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                });
            };

            $scope.readFilePush = function(file) {
                $scope.fileName = file;
                $http.get(READ_FILE_URL + file).success(function(data) {
                    $scope.file = $sce.trustAsHtml($scope.removeJsonQuotes(data));
                    $scope.scrollToBottomOnReadFile();

                    var eventSource = new EventSource(GET_FILE_DIFF_PUSH);
                    eventSource.onmessage = function(event) {
                        $scope.file += event.data;
                        $scope.file = $sce.trustAsHtml($scope.file);
                        $scope.$digest();
                        $scope.scrollToBottom()
                    };

                    eventSource.onopen = function(event) {
                        // receiving push notifications
                    }

                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                });
            };

            $scope.readFilePoll = function(file) {
                $scope.fileName = file;
                $http.get(READ_FILE_URL + file).success(function(data) {
                    var actualData = $scope.removeJsonQuotes(data);
                    if (actualData == '') {
                        $scope.file = $sce.trustAsHtml("<pre>Empty</pre>");
                    } else {
                        $scope.file = $sce.trustAsHtml(actualData);
                    }

                    $scope.scrollToBottomOnReadFile();

                    $interval.cancel($scope.intervalPromise);

                    $scope.intervalPromise = $interval(function(){
                        var selectedDir;
                        var selectedFile;

                        // checking if SELECTED DIR has not changed
                        $http.get(GET_CURR_PATH_URL).success(function(data) {
                            selectedDir = $scope.removeJsonQuotes(data);
                            if (selectedDir != $scope.currentPath) {
                                $scope.loadData();
                            }
                        }).error(function(data, status, headers, config) {
                            $scope.showError(data);
                            $interval.cancel($scope.intervalPromise);
                        });

                        // checking if SELECTED FILE has not changed
                        $http.get(GET_SELECTED_FILE_NAME_URL).success(function(data) {
                            selectedFile = data.name;
                            $scope.selectedFileSize = data.size;
                            if ($scope.selectedFileSize > $scope.MAX_FILE_SIZE) {
                                noty.error("Warning", "Selected file is too large and will not be updated automatically");
                                $interval.cancel($scope.intervalPromise);
                            }
                            if (selectedFile != $scope.fileName) {
                                $scope.readFilePoll(selectedFile);
                                return;
                            }
                        }).error(function(data, status, headers, config) {
                            $scope.showError(data);
                            $interval.cancel($scope.intervalPromise);
                        });

                        // getting file changes
                        $http.get(GET_FILE_DIFF_POLL).success(function(data) {
                            var actualData = $scope.removeJsonQuotes(data);
                            if (actualData != '') {
                                $scope.file += actualData;
                                $scope.file = $sce.trustAsHtml($scope.file);
                                $scope.scrollToBottom();
                            }
                        }).error(function(data, status, headers, config) {
                            $scope.showError(data);
                            $interval.cancel($scope.intervalPromise);
                        });
                    }, POLLING_INTERVAL);

                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                    $interval.cancel($scope.intervalPromise);
                });
            };

            $scope.readFileOnce = function(file) {
                $scope.fileName = file;
                $http.get(READ_FILE_URL + file).success(function(data) {
                    var actualData = $scope.removeJsonQuotes(data);
                    if (actualData == '') {
                        $scope.file = $sce.trustAsHtml("<pre>Empty</pre>");
                    } else {
                        $scope.file = $sce.trustAsHtml(actualData);
                    }

                    $scope.scrollToBottomOnReadFile();

                    $interval.cancel($scope.intervalPromise);

                    $scope.intervalPromise = $interval(function(){

                        // getting file changes
                        $http.get(GET_FILE_DIFF_POLL).success(function(data) {
                            var actualData = $scope.removeJsonQuotes(data);
                            if (actualData != '') {
                                $scope.file += actualData;
                                $scope.file = $sce.trustAsHtml($scope.file);
                                $scope.scrollToBottom();
                            }
                        }).error(function(data, status, headers, config) {
                            $scope.showError(data);
                            $interval.cancel($scope.intervalPromise);
                        });
                    }, POLLING_INTERVAL);

                }).error(function(data, status, headers, config) {
                    $scope.showError(data);
                    $interval.cancel($scope.intervalPromise);
                });
            };

            $scope.testHeaders = function () {
                $http.get(REST_URL + 'test', {
                    headers: {'my-header': 'Basic QWxhZGRpbjpvcGVuIHNlc2FtZQ=='}
                }).success(function(data) {
                    $log.info(data);
                });
            }

            $scope.scrollToBottom = function() {
                $interval(function(){
                    var height;
                    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
                    if (isFirefox == true) {
                        height = window.scrollMaxY - 10;
                    } else {
                        height = document.body.scrollHeight;
                    }
                    window.scrollTo(0,height);
                }, 50, 1);

                //var loadingGif = document.getElementById('loadingGif');
                //loadingGif.scrollIntoView(false);
            }

            $scope.scrollToBottomOnReadFile = function() {
                window.scrollTo(0,0);
                $interval(function(){
                    var height;
                    var isFirefox = typeof InstallTrigger !== 'undefined';   // Firefox 1.0+
                    if (isFirefox == true) {
                        height = window.scrollMaxY - 10;
                    } else {
                        height = document.body.scrollHeight;
                    }
                    window.scrollTo(0,height);
                }, 50, 1);

                //var loadingGif = document.getElementById('loadingGif');
                //loadingGif.scrollIntoView(false);
            }

            $scope.removeJsonQuotes = function(string) {
                return string.replace(/^"|"$/g, '');
            }

            $scope.showError = function (string) {
                noty.error('Failed', string.substr(0, 200));
                $scope.file = $sce.trustAsHtml("<pre style='color: red;'>" + string + "</pre>");
            }

            // auto scroll for content

            function getScrollTop() {
                if (typeof window.pageYOffset !== 'undefined' ) {
                    // Most browsers
                    return window.pageYOffset;
                }

                var d = document.documentElement;
                if (d.clientHeight) {
                    // IE in standards mode
                    return d.scrollTop;
                }

                // IE in quirks mode
                return document.body.scrollTop;
            }

            window.onscroll = function() {
                var box = document.getElementById('sidebar'),
                    scroll = getScrollTop();
                    box.style.top = (scroll + 2) + "px";
            };

            // on startup
            noty.info('FS manager loaded', "RequireJS has managed to load this angular/bootstrap mess");
            $scope.loadData();
        }]);
});