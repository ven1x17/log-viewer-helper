define([
    'require',
    'angular',
    'app',
    'jBootstrap',
    'angularBootstrap',
    'angularResource',
    'angularAnimate'
], function (require, ng) {
    'use strict';

    require(['domReady!'], function (document) {
        ng.bootstrap(document, ['app']);
    });
});