package org.vx.logs.file;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.monitor.FileAlterationListener;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.apache.commons.lang.StringUtils;
import org.vx.logs.FSManager;

public class ChangesObserver {
	private FileAlterationObserver observer;
	private ScheduledExecutorService handlerExecutorService;

	public static final int INTERVAL = 200;
	private File selectedFile;
	private String fileContents;

	private String changes;
	private AtomicBoolean selectedFileChanged = new AtomicBoolean(false);
	private long observerStartTime = 0;
	private static final int OBSERVER_TIMEOUT = 10000;

	public String getChangesInHtml() {
		observerStartTime = System.currentTimeMillis();
		if (!selectedFileChanged.get() || selectedFile == null) {
			return "";
		}
		try {
			String changedFile = FSManager.readFileFromEnd(selectedFile, FSManager.SHOW_LINES);
			changes = StringUtils.difference(fileContents, changedFile);
			fileContents = changedFile;
		}
		catch (IOException e) {
			e.printStackTrace();
		}

		String changesInHtml = FSManager.convertToHtml(changes);
		changes = "";
		selectedFileChanged.set(false);
		return changesInHtml;
	}

	public File getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(File selectedFile) throws Exception {
		this.selectedFile = selectedFile;
		if (selectedFile == null) {
			return;
		}
		fileContents = FSManager.readFileFromEnd(selectedFile, FSManager.SHOW_LINES);

		if (observer != null) {
			observer.destroy();
		}
		if (handlerExecutorService != null) {
			handlerExecutorService.shutdown();
		}
		observer = new FileAlterationObserver(selectedFile.getParent(), FileFilterUtils.nameFileFilter(selectedFile.getName()));
		observer.addListener(new FileListener());
		observer.initialize();
		observerStartTime = System.currentTimeMillis();

		handlerExecutorService = Executors.newSingleThreadScheduledExecutor();
		handlerExecutorService.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				long passedTime = System.currentTimeMillis() - observerStartTime;
				if (passedTime >= OBSERVER_TIMEOUT) {
					handlerExecutorService.shutdown();
					selectedFileChanged.set(true);
					System.out.println("Shutting down observer...");
				} else {
					observer.checkAndNotify();
				}
			}
		}, INTERVAL, INTERVAL, TimeUnit.MILLISECONDS);
	}

	private class FileListener implements FileAlterationListener {
		@Override
		public void onStart(FileAlterationObserver observer) {
		}

		@Override
		public void onDirectoryCreate(File directory) {

		}

		@Override
		public void onDirectoryChange(File directory) {

		}

		@Override
		public void onDirectoryDelete(File directory) {

		}

		@Override
		public void onFileCreate(File file) {

		}

		@Override
		public void onFileChange(File file) {
			selectedFileChanged.set(true);
		}

		@Override
		public void onFileDelete(File file) {

		}

		@Override
		public void onStop(FileAlterationObserver observer) {

		}
	}
}
