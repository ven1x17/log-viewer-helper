package org.vx.logs;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.vx.logs.file.ChangesObserver;

public class FileChangePushServlet extends HttpServlet {

	@Autowired
	private ChangesObserver changesObserver;

	//initializing spring context
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
				config.getServletContext());
	}

	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//content type must be set to text/event-stream
		response.setContentType("text/event-stream");

		//encoding must be set to UTF-8
		response.setCharacterEncoding("UTF-8");

		PrintWriter writer = response.getWriter();
		String changes = changesObserver.getChangesInHtml();
		if (!"".equals(changes)) {
			writer.write("data: " + changes + "\n\n");
		}
		writer.close();

	}

}
