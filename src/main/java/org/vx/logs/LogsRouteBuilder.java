/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.vx.logs;

import java.util.List;

import org.apache.camel.model.rest.RestBindingMode;

/**
 * Define REST services using the Camel REST DSL
 */
public class LogsRouteBuilder extends org.apache.camel.builder.RouteBuilder {

    @Override
    public void configure() throws Exception {

        // configure we want to use servlet as the component for the rest DSL
        // and we enable json binding mode
        restConfiguration().component("servlet").bindingMode(RestBindingMode.json)
            // and output using pretty print
            .dataFormatProperty("prettyPrint", "true")
            // setup context path and port number that Apache Tomcat will deploy
            // this application with, as we use the servlet component, then we
            // need to aid Camel to tell it these details so Camel knows the url
            // to the REST services.
            // Notice: This is optional, but needed if the RestRegistry should
            // enlist accurate information. You can access the RestRegistry
            // from JMX at runtime
            .contextPath("rest").port(8080);

        rest("/fs").description("FS rest service")
                .consumes("application/json").produces("application/json")

                .get("/dirName").description("Get current directory name").outType(String.class)
                .to("bean:fsManager?method=getCurrentDir(${header.credentials})")

                .get("/selectedFile").description("Get currently selected file").outType(String.class)
                .to("bean:fsManager?method=getSelectedFile(${header.credentials})")

                .get("/ls").description("Get current directory contents").outType(List.class)
                .to("bean:fsManager?method=getCurrentDirList(${header.credentials})")

                .get("/up").description("goToParentFolder").outType(List.class)
                .to("bean:fsManager?method=goToParentFolder(${header.credentials})")

                .get("/cd/{folder}").description("goToFolder").outType(List.class)
                .to("bean:fsManager?method=goToFolder(${header.credentials}, ${header.folder})")

                .get("/read/{file}").description("read file contents").outType(String.class)
                .to("bean:fsManager?method=readFile(${header.credentials}, ${header.file})")

                .get("/diff").description("read selected file changes").outType(String.class)
                .to("bean:fsManager?method=getSelectedFileDiff(${header.credentials})")

                .get("/test").description("tewst").outType(String.class)
                .to("bean:fsManager?method=testMethod(${header.credentials}, ${header.my-header})");
    }

}