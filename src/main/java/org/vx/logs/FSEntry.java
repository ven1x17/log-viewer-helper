package org.vx.logs;

import java.util.Objects;

public class FSEntry implements Comparable<FSEntry> {

	private String name;
	private boolean isDir;
	private long lastModificationAgeInMinutes;
	private long size = -1;

	public FSEntry(String name, boolean isDir) {
		this.name = name;
		this.isDir = isDir;
	}

	public FSEntry(String name, boolean isDir, long lastModificationAgeInMinutes) {
		this.name = name;
		this.isDir = isDir;
		this.lastModificationAgeInMinutes = lastModificationAgeInMinutes;
	}

	public FSEntry() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isDir() {
		return isDir;
	}

	public void setDir(boolean isDir) {
		this.isDir = isDir;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public long getLastModificationAgeInMinutes() {
		return lastModificationAgeInMinutes;
	}

	public void setLastModificationAgeInMinutes(long lastModificationAgeInMinutes) {
		this.lastModificationAgeInMinutes = lastModificationAgeInMinutes;
	}

	@Override
	public int compareTo(FSEntry o) {
		if (o.isDir() && !isDir) {
			return 1;
		}
		if (!o.isDir() && isDir) {
			return -1;
		}
		return name.compareToIgnoreCase(o.getName());
	}

	@Override
	public int hashCode() {
		return Objects.hash(name, isDir, lastModificationAgeInMinutes, size);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		final FSEntry other = (FSEntry) obj;
		return Objects.equals(this.name, other.name) && Objects.equals(this.isDir, other.isDir) && Objects
				.equals(this.lastModificationAgeInMinutes, other.lastModificationAgeInMinutes) && Objects.equals(this.size, other.size);
	}

	@Override
	public String toString() {
		return name;
	}
}
