package org.vx.logs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.ReversedLinesFileReader;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.springframework.beans.factory.annotation.Autowired;
import org.vx.logs.file.ChangesObserver;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

public class FSManager {

	public static final int SHOW_LINES = 2000;
	public static final int MAX_FILE_SIZE = 50; //mb
	public static final int BYTES_IN_MB = 1_000_000;
	@Autowired
	private ChangesObserver changesObserver;

	private static final File USER_HOME = FileUtils.getUserDirectory();
	private File currentDir = USER_HOME;
	private File selectedFile;
	private int num = 0;

	private static final boolean SECURITY_ENABLED = false;
	private static final String VALID_CREDENTIALS = "Honey;Badger";

	public String getCurrentDir(String credentials) {
		checkCredentials(credentials);
		return currentDir.getPath();
	}

	public List<FSEntry> getCurrentDirList(String credentials) {
		checkCredentials(credentials);
		List<FSEntry> unsorted = Lists.transform(Lists.newArrayList(currentDir.listFiles()), new Function<File, FSEntry>() {
			@Override
			public FSEntry apply(File f) {
				long diff = Minutes.minutesBetween(new DateTime(new Date(f.lastModified())), DateTime.now()).getMinutes();
				return new FSEntry(f.getName(), f.isDirectory(), diff);
			}
		});
		List<FSEntry> result = Lists.newArrayList(unsorted);
		Collections.sort(result);
		return result;
	}

	public List<FSEntry> goToParentFolder(String credentials) {
		checkCredentials(credentials);
		if ("/".equals(currentDir.getPath())) {
			throw new IllegalArgumentException("You are already at root folder");
		} else {
			String currentDirStr = currentDir.getPath();
			currentDirStr = currentDirStr.substring(0, currentDirStr.lastIndexOf(IOUtils.DIR_SEPARATOR_UNIX));
			if ("".equals(currentDirStr)) {
				currentDirStr = "/";
			}
			currentDir = new File(currentDirStr);
		}
		return getCurrentDirList(credentials);
	}

	public List<FSEntry> goToFolder(String credentials, String folder) {
		checkCredentials(credentials);
		String currentDirStr = currentDir.getPath();
		currentDirStr += IOUtils.DIR_SEPARATOR_UNIX + folder;
		File newDir = new File(currentDirStr);
		if (newDir.exists()) {
			currentDir = newDir;
		} else {
			throw new IllegalArgumentException("Dir doesn't exist");
		}
		return getCurrentDirList(credentials);
	}

	public String readFile(String credentials, String fileName) throws Exception {
		checkCredentials(credentials);
		selectedFile = new File(currentDir.getPath() + IOUtils.DIR_SEPARATOR_UNIX + fileName);

		try {
			double length = selectedFile.length();
			if (length > MAX_FILE_SIZE * BYTES_IN_MB) {
				return handleLargeFile();
			}

			changesObserver.setSelectedFile(selectedFile);

			return convertToHtml(getSelectedFileContent());
		}
		catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	private String handleLargeFile() throws Exception {
		changesObserver.setSelectedFile(null);
		return convertToHtml(getSelectedFileContent());
	}

	public String getSelectedFileDiff(String credentials) {
		checkCredentials(credentials);

		return changesObserver.getChangesInHtml();
	}

	public FSEntry getSelectedFile(String credentials) {
		checkCredentials(credentials);
		FSEntry fsEntry = new FSEntry(selectedFile.getName(), false);
		fsEntry.setSize(selectedFile.length());

		long diff = Minutes.minutesBetween(new DateTime(new Date(selectedFile.lastModified())), DateTime.now()).getMinutes();
		fsEntry.setLastModificationAgeInMinutes(diff);

		return fsEntry;
	}

	private void checkCredentials(String credentials) {
		if (SECURITY_ENABLED && credentials == null) {
			throw new IllegalArgumentException("wrong credentials");
		}
	}

	public static String convertToHtml(String plainText) {
		return plainText
				.replace("<", "&lt;")
				.replace(">", "&gt;")
				.replace("\n", "<br>")
				.replace("\"", "&quot;")
				.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
	}

	public String testMethod(String credentials, Object header) {
		System.out.println(header);
		return credentials.equals(VALID_CREDENTIALS) ? "authenticated" : "wrong credentials";
	}

	private String getSelectedFileContent() throws IOException {
		return readFileFromEnd(selectedFile, SHOW_LINES);
	}

	public static String readFileFromEnd(File file, int linesCount) throws IOException {
		StringBuilder content = new StringBuilder("");
		List<String> contentFromEndAsList = Lists.newArrayListWithExpectedSize(linesCount);

		ReversedLinesFileReader reader = new ReversedLinesFileReader(file);
		for (int i = 0; i < linesCount; i++) {
			String parsedLine = reader.readLine();
			if (parsedLine == null) {
				break;
			}
			contentFromEndAsList.add(parsedLine);
		}

		Collections.reverse(contentFromEndAsList);
		for (String line : contentFromEndAsList) {
			content.append(line);
			content.append('\n');
		}

		return content.toString();
	}

	public static int countLines(File file) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(file));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars = 0;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		}
		finally {
			is.close();
		}
	}
}
