Java webapp to provide ability to view contents of any file in a unix fs.

Depending on selected version file content updates may be refreshed in real time.
File tree entries that were modified in the last 24h are highlighted in red.

Only one session per instance is supported - it was a rest compliance / ease of use decision.
This way you can collaborate with team members by pointing to exact log files - state is shared.

-----------------------

![Screenshot](screenshots/screenshot1.png)